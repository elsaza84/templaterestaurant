<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CarouselRepository")
 */
class Carousel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="carousels")
     */
    private $post;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CarouselDetail", mappedBy="carousel")
     */
    private $carouselDetails;

    public function __construct()
    {
        $this->carouselDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(?Post $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    /**
     * @return Collection|CarouselDetail[]
     */
    public function getCarouselDetails(): Collection
    {
        return $this->carouselDetails;
    }

    public function addCarouselDetail(CarouselDetail $carouselDetail): self
    {
        if (!$this->carouselDetails->contains($carouselDetail)) {
            $this->carouselDetails[] = $carouselDetail;
            $carouselDetail->setCarousel($this);
        }

        return $this;
    }

    public function removeCarouselDetail(CarouselDetail $carouselDetail): self
    {
        if ($this->carouselDetails->contains($carouselDetail)) {
            $this->carouselDetails->removeElement($carouselDetail);
            // set the owning side to null (unless already changed)
            if ($carouselDetail->getCarousel() === $this) {
                $carouselDetail->setCarousel(null);
            }
        }

        return $this;
    }
}
