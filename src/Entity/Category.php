<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="category")
     */
    private $posts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TypeMenu", mappedBy="category")
     */
    private $typeMenus;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
        $this->typeMenus = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setCategory($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getCategory() === $this) {
                $post->setCategory(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return Collection|TypeMenu[]
     */
    public function getTypeMenus(): Collection
    {
        return $this->typeMenus;
    }

    public function addTypeMenu(TypeMenu $typeMenu): self
    {
        if (!$this->typeMenus->contains($typeMenu)) {
            $this->typeMenus[] = $typeMenu;
            $typeMenu->setCategory($this);
        }

        return $this;
    }

    public function removeTypeMenu(TypeMenu $typeMenu): self
    {
        if ($this->typeMenus->contains($typeMenu)) {
            $this->typeMenus->removeElement($typeMenu);
            // set the owning side to null (unless already changed)
            if ($typeMenu->getCategory() === $this) {
                $typeMenu->setCategory(null);
            }
        }

        return $this;
    }
}
