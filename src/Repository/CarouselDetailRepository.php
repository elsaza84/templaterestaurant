<?php

namespace App\Repository;

use App\Entity\CarouselDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CarouselDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method CarouselDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method CarouselDetail[]    findAll()
 * @method CarouselDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarouselDetailRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CarouselDetail::class);
    }

    // /**
    //  * @return CarouselDetail[] Returns an array of CarouselDetail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CarouselDetail
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
