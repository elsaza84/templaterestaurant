<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Category;

class CategoryExtension extends AbstractExtension
{
    private $objectManager;

    public function __construct (ObjectManager $objectManager) {
        $this->objectManager = $objectManager;
    }

    public function getFunctions(): array
    {
        return [
            //nom dee ma fonction
            new TwigFunction('get_categories', [$this, 'getCategories']),
        ];
    }

    public function getCategories()
    {// manager prend le repository de la'entité category et trouve tout
        return $this->objectManager->getRepository(Category::class)->findAll();
    }
}
