<?php

namespace App\Twig;

use App\Entity\Post;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use Doctrine\Common\Persistence\ObjectManager;

class PostExtension extends AbstractExtension
{

    private $objectManager;

    public function __construct (ObjectManager $objectManager) {
        $this->objectManager = $objectManager;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_posts', [$this, 'get_posts']),
        ];
    }

    public function get_posts()
    {
        return $this->objectManager->getRepository(Post::class)->findAll();
    }
}
