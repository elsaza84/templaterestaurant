<?php

namespace App\Twig;

use Twig\TwigFilter;
use Twig\TwigFunction;
use App\Entity\TypeMenu;
use Twig\Extension\AbstractExtension;
use Doctrine\Common\Persistence\ObjectManager;

class TypeMenuExtension extends AbstractExtension
{
  
    private $objectManager;

    public function __construct (ObjectManager $objectManager) {
        $this->objectManager = $objectManager;
    }
    
    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_typeMenus', [$this, 'get_typeMenus']),
        ];
    }

    public function get_typeMenus()
    {
        return $this->objectManager->getRepository(TypeMenu::class)->findAll();
    }
}
