<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    private $passwordEncoder;

        public function __construct(UserPasswordEncoderInterface $passwordEncoder)
        {
            $this->passwordEncoder = $passwordEncoder;
        }
    
    
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

     
            $user1 = new User(); 

            $user1->setFirstname($faker->name)
                ->setLastname($faker->name)
                ->setEmail($faker->email)
                ->setPassword($this->passwordEncoder->encodePassword(
                                 $user1,
                                 'elsaza84700'
                             ))
                ->setRoles(array('ROLE_ADMIN'));
                $manager->persist($user1);
                $manager->flush();
                
                
                $user2 = new User(); 



                $user2->setFirstname($faker->name)
                ->setLastname($faker->name)
                ->setEmail($faker->email)
                ->setPassword($this->passwordEncoder->encodePassword(
                                 $user2,
                                 'elsaza84800'
                             ))
                ->setRoles(array('ROLE_USER'));
                $manager->persist($user2);
                $manager->flush();
                
        
       
    }
}
