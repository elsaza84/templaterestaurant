<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Menu;
use App\Entity\Post;
use App\Entity\Carousel;
use App\Entity\Category;
use App\Entity\TypeMenu;
use Cocur\Slugify\Slugify;
use App\Entity\CarouselDetail;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    { //initialisation de l'objet Faker
        $faker = Factory::create();
        $slug = new Slugify();
        
   
      //creation des categories
        for($i=1; $i<=4; $i++){
            $title = $faker->name;
            $sluggy = $slug->slugify($title);
            $category = new Category();
            $category->setTitle($title)
                ->setDescription($faker->sentence())
                ->setImage($faker->imageUrl(640, 480))
                ->setSlug($sluggy);
        //creation des posts
                for ($p = 0; $p < rand(3,5); $p++) {
                    $post = (new Post())
                    ->setTitle($faker->country)
                    ->setContent($faker->text)
                    ->setCategory($category);
                    $manager->persist($post);
        //creation des carousels
                    $carousel = (new Carousel)
                    ->setName($faker->sentence(7))
                    ->setPost($post)
                    ->setPublished($faker->boolean(50));
                    $manager->persist($carousel);
        //creation des carouselDetails
                    for($cd=1; $cd <=5 ;$cd++){
                        $carouselDetail = new CarouselDetail();
                        $carouselDetail->setImage($faker->imageUrl(650,350))
                                       ->setDescription($faker->sentence(10))
                                       ->setCarousel($carousel);
                    
                                       $manager->persist($carouselDetail);
                    }
        //creation des TypeMenus   
                        $typeMenu = new TypeMenu();
                        $typeMenu->setName($faker->state)
                                ->setPublished($faker->boolean(50))
                                ->setUpdatedAt( new \DateTime())
                                ->setDescription($faker->paragraph(3))
                                ->setImage($faker->imageUrl(350,350))
                                ->setCategory($category);
                                
                                $manager->persist($typeMenu);

        //creation de s
                                for( $m = 1; $m <=3 ; $m++){
                                             $menu = new Menu();
                                            $name= $faker->sentence(4);
                                             $slug= new Slugify();
                                             $sluggy= $slug->slugify($name);
                                            $menu->setTypeMenu($typeMenu)
                                            ->setSlug($sluggy)
                                            -> setName($name)
                                            ->setPrice($faker->randomNumber(2))
                                            ->setContent($faker->paragraph(6))
                                            ->setImage($faker->imageUrl(450,450))
                                            ->setUpdatedAt( new \DateTime());
                                    
                                            $manager->persist($menu);
                                }
            
            
            
            
                    
                
                }
                $manager->persist($category);
        } 
        

        $manager->flush();
    }
}
