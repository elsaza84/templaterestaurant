<?php

namespace App\Controller;

use App\Entity\TypeMenu;
use App\Form\TypeMenuType;
use App\Repository\TypeMenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/type/menu")
 */
class TypeMenuController extends AbstractController
{
    /**
     * @Route("/", name="type_menu_index", methods={"GET"})
     */
    public function index(TypeMenuRepository $typeMenuRepository): Response
    {
        return $this->render('type_menu/index.html.twig', [
            'type_menus' => $typeMenuRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="type_menu_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typeMenu = new TypeMenu();
        $form = $this->createForm(TypeMenuType::class, $typeMenu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typeMenu);
            $entityManager->flush();

            return $this->redirectToRoute('type_menu_index');
        }

        return $this->render('type_menu/new.html.twig', [
            'type_menu' => $typeMenu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_menu_show", methods={"GET"})
     */
    public function show(TypeMenu $typeMenu): Response
    {
        return $this->render('type_menu/show.html.twig', [
            'type_menu' => $typeMenu,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="type_menu_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypeMenu $typeMenu): Response
    {
        $form = $this->createForm(TypeMenuType::class, $typeMenu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('type_menu_index', [
                'id' => $typeMenu->getId(),
            ]);
        }

        return $this->render('type_menu/edit.html.twig', [
            'type_menu' => $typeMenu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_menu_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TypeMenu $typeMenu): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeMenu->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typeMenu);
            $entityManager->flush();
        }

        return $this->redirectToRoute('type_menu_index');
    }
}
