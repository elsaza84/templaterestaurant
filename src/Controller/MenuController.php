<?php

namespace App\Controller;

use App\Entity\Menu;
use App\Form\MenuType;
use App\Repository\MenuRepository;
use App\Repository\TypeMenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/menu")
 */
class MenuController extends AbstractController
{
    /**
     * @Route("/", name="menu.index", methods={"GET"})
     */
    public function index(MenuRepository $menuRepository, TypeMenuRepository $typeMenuRepository): Response
    {
        return $this->render('menu_home/index.html.twig', [
            'menus' => $menuRepository->findAll(),
            'type_menus' => $typeMenuRepository->findAll()
        ]);
    }
}
