<?php

namespace App\Controller;


use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\CategoryRepository;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
public function index(CategoryRepository $categrepo)
    {
       

        return $this->render('home/index.html.twig');
    }
}
