<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190514082250 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE carousel_detail ADD carousel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE carousel_detail ADD CONSTRAINT FK_5CCE8ECFC1CE5B98 FOREIGN KEY (carousel_id) REFERENCES carousel (id)');
        $this->addSql('CREATE INDEX IDX_5CCE8ECFC1CE5B98 ON carousel_detail (carousel_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE carousel_detail DROP FOREIGN KEY FK_5CCE8ECFC1CE5B98');
        $this->addSql('DROP INDEX IDX_5CCE8ECFC1CE5B98 ON carousel_detail');
        $this->addSql('ALTER TABLE carousel_detail DROP carousel_id');
    }
}
